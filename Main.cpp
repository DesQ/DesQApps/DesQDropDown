/**
 * This file is a part of DesQ DropDown.
 * DesQ DropDown provides a simple drop-down style widget for DesQ
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Global.hpp"
#include "DropDown.hpp"

#include <signal.h>

#include <DFApplication.hpp>
#include <DFSettings.hpp>
#include <DFUtils.hpp>
#include <DFXdg.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>

#include <desq/Utils.hpp>

DFL::Settings   *ddSett     = nullptr;
WQt::Registry   *wlRegistry = nullptr;
WQt::LayerShell *lyrShell   = nullptr;

int main( int argc, char **argv ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/DropDown.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    DFL::SHOW_WARNING_ON_CONSOLE = true;

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ DropDown started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    qputenv( "QT_WAYLAND_USE_BYPASSWINDOWMANAGERHINT", QByteArrayLiteral( "1" ) );
    DFL::Application app( argc, argv );

    app.setOrganizationName( "DesQ" );
    app.setApplicationName( "DropDown" );
    app.setApplicationVersion( PROJECT_VERSION );
    app.setDesktopFileName( "desq-dropdown" );

    app.setQuitOnLastWindowClosed( false );

    app.interceptSignal( SIGTERM, nullptr );

    if ( app.lockApplication() ) {
        ddSett = DesQ::Utils::initializeDesQSettings( "DropDown", "DropDown" );

        if ( WQt::Utils::isWayland() ) {
            wlRegistry = new WQt::Registry( WQt::Wayland::display() );

            /** Just to be safe */
            if ( wlRegistry ) {
                wlRegistry->setup();

                /** Prepare the LayerShell */
                if ( wlRegistry->waitForInterface( WQt::Registry::LayerShellInterface ) ) {
                    lyrShell = wlRegistry->layerShell();
                }
            }
        }

        DesQDropDown *desqdd = new DesQDropDown();

        QObject::connect( &app, &DFL::Application::messageFromClient, desqdd, &DesQDropDown::handleMessages );

        QProcess::startDetached(
            "notify-send", {
                "-i", "desq-dropdown",
                "-a", "DesQ DropDown",
                "-t", "2000",
                "DesQ DropDown Started",            /* Title */
                "DesQ DropDown has started and is running in the background. Press F12 to activate it."
            }
        );

        return app.exec();
    }

    if ( app.isRunning() ) {
        qDebug() << "DesQ DropDown is already running. Pinging it...";
        qDebug() << app.messageServer( "Toggle" );

        return 0;
    }

    return 0;
}
