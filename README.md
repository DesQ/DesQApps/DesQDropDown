# DesQ DropDown
## A Quake-style DropDown app capable of showing widgets.

A simple drop-down terminal app which may used to access quick settings (Android style), and other widgets.
All the widgets are Qt Plugins. Installing DesQFiles and DesQTerm gives the file manager and terminal plugins for the DropDown.

### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQApps/DesQDropDown.git DesQDropDown`
- Enter the `DesQDropDown` folder
  * `cd DesQDropDown`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Dependencies:
* Qt5 (qtbase5-dev [5.15.2+dfsg-2], qtbase5-dev-tools [5.15.2+dfsg-2])
* WlrootsQt (https://gitlab.com/desktop-frameworks/wayqt)
* libdesq (https://gitlab.com/DesQ/libdesq)


## My System Info
* OS:				Debian Sid
* Qt:				Qt5 5.15.2


### Upcoming
* Any other feature you request for... :)
