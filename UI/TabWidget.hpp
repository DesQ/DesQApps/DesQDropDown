/**
 * This file is a part of DesQ DropDown.
 * DesQ DropDown provides a simple drop-down style widget for DesQ
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

class DropDownTabs : public QWidget {
    Q_OBJECT

    public:
        DropDownTabs( QWidget *parent );

        /* Add a new tab */
        int addTab( QWidget *, QIcon, QString );

        /* Tab count */
        int count();

        /* Currently visible widget */
        QWidget * currentWidget();

        /* Widget at position i */
        QWidget * widget( int );

        /* Get/Set the current index */
        int currentIndex();
        void setCurrentIndex( int );

        /* Get/Set tab title */
        QString tabText( int );
        void setTabText( int, QString );

    private:
        QStackedWidget *stack;
        QVBoxLayout *tabsLyt;
        QButtonGroup *tabs;
        QStringList titles;

    Q_SIGNALS:
        void closeWindow();
        void currentChanged( int );
        void changeWindowTitle( QString );
};
