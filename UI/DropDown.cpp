/**
 * This file is a part of DesQ DropDown.
 * DesQ DropDown provides a simple drop-down style widget for DesQ
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Global.hpp"
#include "DropDown.hpp"

#include <desqui/DropDownPlugin.hpp>
#include <desq/desq-config.h>

#include <DFApplication.hpp>
#include <DFSettings.hpp>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/LayerShell.hpp>

DesQDropDown::DesQDropDown( QWidget *parent ) : QMainWindow( parent ) {
    relWidth       = ddSett->value( "WindowWidth" );
    relHeight      = ddSett->value( "WindowHeight" );
    mUseLayerShell = ddSett->value( "UseLayerShell" );

    connect( qApp->primaryScreen(), &QScreen::geometryChanged, this, &DesQDropDown::autoResize );

    setWindowIcon( QIcon::fromTheme( "desq-dropdown" ) );
    setWindowTitle( "DesQ DropDown" );

    setAttribute( Qt::WA_TranslucentBackground );
    setWindowFlags( Qt::FramelessWindowHint );

    /** BypassWindowManagerHint makes sense only when we have wlr-layer-shell support */
    if ( mUseLayerShell and lyrShell ) {
        setWindowFlags( Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
    }

    tabs = new DropDownTabs( this );

    connect( ddSett, &DFL::Settings::settingChanged, this, &DesQDropDown::modifySetting );

    connect( tabs,   &DropDownTabs::closeWindow,     this, &DesQDropDown::hide );
    connect( tabs,   &DropDownTabs::currentChanged,  this, &DesQDropDown::handleCurrentChanged );

    setCentralWidget( tabs );
}


void DesQDropDown::show() {
    autoResize();
    QMainWindow::showNormal();

    /**
     * We want layer shell, and have it too.
     * In case we don't have lyrShell, no problems, the view will be shown normally.
     */
    if ( mUseLayerShell and lyrShell ) {
        WQt::LayerShell::LayerType lyr = WQt::LayerShell::Top;
        lyrSurf = lyrShell->getLayerSurface(
            windowHandle(),     // Window Handle
            nullptr,            // wl_output object - for multi-monitor support
            lyr,                // Background layer
            "drop-down"         // Dummy namespace
        );

        lyrSurf->setAnchors( WQt::LayerSurface::SurfaceAnchor::Top );

        /** Size of our surface */
        lyrSurf->setSurfaceSize( size() );

        /** No gap anywhere */
        lyrSurf->setMargins( QMargins( 0, 0, 0, 0 ) );

        /** Nothing should disturb us */
        lyrSurf->setExclusiveZone( -1 );

        lyrSurf->setKeyboardInteractivity( WQt::LayerSurface::Exclusive );

        /** Commit to our choices */
        lyrSurf->apply();
    }

    if ( not pluginsLoaded ) {
        QTimer::singleShot( 100, this, &DesQDropDown::loadPlugins );
    }

    QMainWindow::setFocus();
    QCoreApplication::processEvents();
}


void DesQDropDown::handleMessages( QString msg, int ) {
    if ( msg == "Toggle" ) {
        if ( isVisible() ) {
            /** If a layer surface was created, delete it */
            if ( lyrSurf ) {
                delete lyrSurf;
                lyrSurf = nullptr;
            }

            /** This this window */
            hide();
        }

        else {
            /** Use our implementation to show this window */
            show();
        }
    }
}


void DesQDropDown::autoResize() {
    // Span the entire width of the screen
    QSize scrSize = qApp->primaryScreen()->size();

    windowSize = QSize( ( int )(scrSize.width() * relWidth), ( int )(scrSize.height() * relHeight) );

    resize( windowSize );
    setFixedSize( windowSize );
}


void DesQDropDown::loadPlugins() {
    QStringList plugins = ddSett->value( "Plugins" );
    QStringList pPaths  = ddSett->value( "PluginPaths" );

    pPaths << PluginPath "dropdown/";
    for ( QString plugName: plugins ) {
        bool loaded = false;
        for ( QString path: pPaths ) {
            QString pluginSo = QString( "%1/lib%2.so" ).arg( path ).arg( plugName );

            if ( QFile::exists( pluginSo ) ) {
                loaded = loadPlugin( pluginSo );
                break;
            }
        }

        if ( loaded == false ) {
            qWarning() << "Failed to load the plugin" << plugName;
        }
    }

    pluginsLoaded = true;

    if ( tabs->count() ) {
        tabs->setCurrentIndex( 0 );
        tabs->currentWidget()->setFocus();
    }
}


bool DesQDropDown::loadPlugin( QString pPath ) {
    QPluginLoader loader( pPath );
    QObject       *pObject = loader.instance();

    if ( pObject ) {
        DesQ::Plugin::DropDown *plugin = qobject_cast<DesQ::Plugin::DropDown *>( pObject );

        if ( plugin == nullptr ) {
            qWarning() << pPath << loader.errorString();
            return false;
        }

        QString name          = plugin->name();
        QIcon   icon          = plugin->icon();
        QWidget *pluginWidget = plugin->widget( tabs );

        qInfo() << "Loading plugin" << name;
        tabs->addTab( pluginWidget, icon, name );
        pluginWidget->setFocus();

        int curIdx = tabs->count();

        if ( curIdx <= 9 ) {
            QShortcut *shortcut = new QShortcut( this );
            shortcut->setContext( Qt::WindowShortcut );
            shortcut->setKey( QKeySequence( Qt::CTRL | (0x30 + curIdx) ) );
            connect(
                shortcut, &QShortcut::activated, [ = ]() {
                    tabs->setCurrentIndex( curIdx - 1 );
                }
            );
        }

        return true;
    }

    else {
        qWarning() << pPath << loader.errorString();
        return false;
    }

    /** Should not reach here */
    return false;
}


void DesQDropDown::handleCurrentChanged( int idx ) {
    QWidget *w = tabs->widget( idx );

    if ( not w ) {
        return;
    }

    w->setFocus();

    QString tTxt = tabs->tabText( idx ).replace( "DesQ ", "" );

    setWindowTitle( "DesQ DropDown | " + tTxt );
}


void DesQDropDown::modifySetting( QString key, QVariant value ) {
    if ( key == "WindowWidth" ) {
        relWidth = ddSett->value( "WindowWidth" );
        autoResize();

        /** Hide the view ... */
        hide();

        /** ... to reshow it */
        show();
    }

    if ( key == "WindowHeight" ) {
        relHeight = ddSett->value( "WindowHeight" );
        autoResize();
    }

    else if ( key == "UseLayerShell" ) {
        mUseLayerShell = value.toBool();

        /** We are to use layerShell, and it's available */
        if ( value.toBool() and lyrShell ) {
            setWindowFlags( Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
        }

        else {
            setWindowFlags( Qt::FramelessWindowHint );
        }

        /** Hide the view ... */
        hide();

        /** ... to reshow it */
        show();
    }
}


void DesQDropDown::paintEvent( QPaintEvent *pEvent ) {
    QMainWindow::paintEvent( pEvent );

    QPainter painter( this );

    painter.save();
    painter.setPen( Qt::NoPen );
    painter.setBrush( QColor( 0, 0, 0, 230 ) );
    painter.drawRect( geometry() );
    painter.restore();

    painter.save();
    painter.setPen( QPen( Qt::gray, 2.0 ) );

    if ( relHeight < 1.0 ) {
        painter.drawLine( QPoint( 0, height() - 1 ), QPoint( width(), height() - 1 ) );
    }

    if ( relWidth < 1.0 ) {
        painter.drawLine( QPoint( 1, 0 ),           QPoint( 1, height() ) );
        painter.drawLine( QPoint( width() - 1, 0 ), QPoint( width() - 1, height() ) );
    }

    painter.restore();

    painter.end();
}


bool DesQDropDown::event( QEvent *event ) {
    /** Hide if the window was deactivated */
    if ( event->type() == QEvent::WindowDeactivate ) {
        hide();
    }

    return QMainWindow::event( event );
}


void DesQDropDown::focusInEvent( QFocusEvent *fEvent ) {
    QMainWindow::focusInEvent( fEvent );
    qApp->processEvents();

    if ( tabs->count() ) {
        tabs->currentWidget()->setFocus();
    }
}


void DesQDropDown::focusOutEvent( QFocusEvent *fEvent ) {
    QMainWindow::focusOutEvent( fEvent );

    qApp->processEvents();

    /** Plugins are not yet loaded, do nothing */
    if ( not pluginsLoaded ) {
        return;
    }

    /** If this window or the tabs widget has focus, do nothing */
    if ( hasFocus() or tabs->hasFocus() ) {
        return;
    }

    /** If tabs are loaded, and one of the widgets of the tab has focus, do nothing */
    if ( tabs->count() and tabs->currentWidget()->hasFocus() ) {
        return;
    }

    hide();
}
