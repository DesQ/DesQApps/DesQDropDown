/**
 * This file is a part of DesQ DropDown.
 * DesQ DropDown provides a simple drop-down style widget for DesQ
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QWidget>
#include "TabWidget.hpp"

namespace WQt {
    class LayerSurface;
}

class DesQDropDown : public QMainWindow {
    Q_OBJECT;

    public:
        DesQDropDown( QWidget *parent = 0 );

    public Q_SLOTS:
        void show();

        void handleMessages( QString, int );

    private:
        qreal relWidth;
        qreal relHeight;
        bool mUseLayerShell;

        QSize windowSize;
        DropDownTabs *tabs;

        bool pluginsLoaded = false;

        /** The layer surface that we'll use to show the drop-down */
        WQt::LayerSurface *lyrSurf = nullptr;

        void autoResize();
        void loadPlugins();

        /** Load a single plugin */
        bool loadPlugin( QString );

        void handleCurrentChanged( int );

        void modifySetting( QString, QVariant );

    protected:
        void paintEvent( QPaintEvent *pEvent );

        bool event( QEvent * ) override;

        void focusInEvent( QFocusEvent * );
        void focusOutEvent( QFocusEvent * );
};
