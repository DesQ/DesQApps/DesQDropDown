/**
 * This file is a part of DesQ DropDown.
 * DesQ DropDown provides a simple drop-down style widget for DesQ
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QShortcut>
#include <DFSettings.hpp>

#include "TabWidget.hpp"

QToolButton * getTabButton( QIcon icon, QButtonGroup *grp, int id ) {
    QToolButton *btn = new QToolButton();

    btn->setIcon( icon );
    btn->setIconSize( QSize( 24, 24 ) );
    btn->setAutoRaise( true );
    btn->setFocusPolicy( Qt::NoFocus );
    btn->setCheckable( true );
    btn->setChecked( false );

    grp->addButton( btn, id );

    return btn;
}


DropDownTabs::DropDownTabs( QWidget *parent ) : QWidget( parent ) {
    setFocusPolicy( Qt::NoFocus );

    /* Stack - We will store the tab widgets here */
    stack = new QStackedWidget( this );

    /* A simple separator */
    QWidget *sep = new QWidget();

    sep->setFixedWidth( 1 );
    sep->setStyleSheet( "background-color: gray;" );

    /* Button Group to handle the tabs */
    tabs = new QButtonGroup( this );
    tabs->setExclusive( true );
    /*  */
    connect(
        tabs, &QButtonGroup::idClicked, [ = ]( int i ) {
            stack->setCurrentIndex( i );
            emit currentChanged( i );
        }
    );

    /* Layout to hold the tabs buttons */
    tabsLyt = new QVBoxLayout();
    tabsLyt->setContentsMargins( QMargins() );
    tabsLyt->setSpacing( 0 );
    tabsLyt->addStretch();

    QToolButton *btn = new QToolButton();

    btn->setIcon( QIcon::fromTheme( "arrow-up" ) );
    btn->setIconSize( QSize( 24, 24 ) );
    btn->setAutoRaise( true );
    btn->setFocusPolicy( Qt::NoFocus );
    /* Tell the main window to hide */
    connect( btn, &QToolButton::clicked, this, &DropDownTabs::closeWindow );

    int lm = 0, rm = 0, bm = 0, tm = 0;

    if ( (double)ddSett->value( "WindowWidth" ) < 1.0 ) {
        lm = 2;
        rm = 2;
    }

    if ( (double)ddSett->value( "WindowHeight" ) < 1.0 ) {
        bm = 2;
    }

    /* Layout for the widget */
    QGridLayout *baseLyt = new QGridLayout();

    baseLyt->setContentsMargins( QMargins( lm, tm, rm, bm ) );
    baseLyt->addWidget( stack, 0, 0, 2, 1 );
    baseLyt->addWidget( sep,   0, 1, 2, 1 );
    baseLyt->addLayout( tabsLyt, 0, 2 );
    baseLyt->addWidget( btn, 1, 2 );

    setLayout( baseLyt );
}


int DropDownTabs::addTab( QWidget *widget, QIcon icon, QString title ) {
    tabsLyt->insertWidget( tabsLyt->count() - 1, getTabButton( icon, tabs, stack->count() ) );
    titles.append( title );

    return stack->addWidget( widget );
}


int DropDownTabs::count() {
    return stack->count();
}


QWidget * DropDownTabs::currentWidget() {
    QWidget *x = stack->currentWidget();

    return x;
}


QWidget * DropDownTabs::widget( int i ) {
    if ( i >= stack->count() ) {
        return nullptr;
    }

    return stack->widget( i );
}


int DropDownTabs::currentIndex() {
    return stack->currentIndex();
}


void DropDownTabs::setCurrentIndex( int i ) {
    if ( i >= stack->count() ) {
        return;
    }

    tabs->button( i )->click();
}


QString DropDownTabs::tabText( int i ) {
    if ( i >= stack->count() ) {
        return QString();
    }

    return titles.value( i );
}


void DropDownTabs::setTabText( int i, QString title ) {
    if ( i >= stack->count() ) {
        return;
    }

    titles[ i ] = title;
}
